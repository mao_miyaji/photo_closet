(function () {

    var unit = 100,
        canvas, context,
        height, width, xAxis, yAxis, cbpd,
        draw;
    var winH, winW;
    var renderTimeHandle;

    window.addEventListener("load", init);
    window.addEventListener("resize", init);

    /**
     * 初期化（読み込み時とリサイズ時に実行）
     */
    function init() {
        canvas = document.getElementById("bgWave");
        cbpd = 20;
        winH = document.documentElement.clientHeight;
        winW = document.documentElement.clientWidth;
        canvas.height = winH - (cbpd * 2);
        canvas.width = winW - (cbpd * 2);
        context = canvas.getContext("2d");
        height = canvas.height;
        width = canvas.width;
        xAxis = Math.floor(height/2);
        yAxis = 0;

        draw();
    }

    /**
     * 描画
     */
    function draw() {
        // キャンバスの描画をクリア
        context.clearRect(0, 0, width, height);
        window.clearTimeout(renderTimeHandle);

        // 波を描画
        drawWave('#10c2cd', 1, 3, 0);
        drawWave('#0000FF', 1, 3, 2);

        // Update the time and draw again
        draw.seconds = draw.seconds + .009;
        draw.t = draw.seconds*Math.PI;
        renderTimeHandle = setTimeout(draw, 35);
    }
    draw.seconds = 0;
    draw.t = 0;

    /**
     * 波の描画
     * @param {string} color 波の色
     * @param {number} alpha 透過度
     * @param {number} zoom 波幅
     * @param {number} delay 波の開始位置
     */
    function drawWave(color, alpha, zoom, delay) {
        // context.fillStyle = color;
        context.strokeStyle = color;
        context.globalAlpha = alpha;

        context.beginPath(); //パスの開始
        drawSine(draw.t / 0.5, zoom, delay);
        // context.lineTo(width + 10, height); //パスをCanvasの右下へ
        // context.lineTo(0, height); //パスをCanvasの左下へ
        // context.closePath() //パスを閉じる
        // context.fill(); //塗りつぶす
        context.stroke()
        // context.restore()
    }

    /**
     * 波のアニメーション設定
     * @param {number} t 時間
     * @param {number} zoom 波幅
     * @param {number} delay 波の開始位置
     */
    function drawSine(t, zoom, delay) {
        var x = t; //時間を横の位置とする
        var y = Math.sin(x)/zoom;
        // context.moveTo(yAxis, unit*y+xAxis); //スタート位置にパスを置く

        // Loop to draw segments (横幅の分、波を描画)
        for (i = yAxis; i <= width + 10; i += 10) {
            x = t+(-yAxis+i)/unit/zoom;
            y = Math.sin(x - delay)/3;
            context.lineTo(i, unit*y+xAxis);
        }
    }

})();