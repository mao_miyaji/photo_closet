// import 'rxjs/Rx'; // adds ALL RxJS statics & operators to Observable

// See node_module/rxjs/Rxjs.js
// Import just the rxjs statics and operators we need for THIS app.

// Statics
// import 'rxjs/add/observable/if';
// import 'rxjs/add/observable/throw';
// import 'rxjs/add/observable/from';
// import 'rxjs/add/observable/interval';
// import 'rxjs/add/observable/forkJoin';
// import 'rxjs/add/observable/timer';
// import 'rxjs/add/observable/of';
// import 'rxjs/add/observable/never';
import 'rxjs/add/observable/empty';
// import 'rxjs/add/observable/fromPromise';
// import 'rxjs/add/observable/bindCallback';
// import 'rxjs/add/observable/range';

// Operators
import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
// import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/retryWhen';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/finally';
// import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/let';
import 'rxjs/add/operator/delay';
// import 'rxjs/add/operator/distinctUntilChanged';
// import 'rxjs/add/operator/do';
import 'rxjs/add/operator/merge';
