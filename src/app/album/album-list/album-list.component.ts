import {Component, OnInit} from '@angular/core';
import {Good} from '../../shared/models';
import {GoodService} from '../../shared/services';

@Component({
    selector: 'app-album-list',
    templateUrl: './album-list.component.html',
    styleUrls: ['./album-list.component.css']
})
export class AlbumListComponent implements OnInit {
    // 品物一覧
    public goods: Good[] = [];
    // 表示中品物
    public selectedItem1: Good;
    public selectedItem2: Good;
    public selectedItem3: Good;
    public selectedItem4: Good;

    public isDisplayItem3: boolean = false;
    public isDisplayItem4: boolean = false;

    constructor(
        private goodService: GoodService
    ) { }

    ngOnInit(): void {
        this.getGoods();
    }

    /**
     * 次の写真へ
     * @param {Good} item
     */
    public turnNextPage(item: Good): void {
        const nextItem = this.goods.find(good => {
            return good.category_id === item.category_id && good.id > item.id;
        });
        if (nextItem) {
            if (nextItem.category_id === 1) {
                this.selectedItem1 = nextItem;
            } else if (nextItem.category_id === 2) {
                this.selectedItem2 = nextItem;
            } else if (nextItem.category_id === 3) {
                this.selectedItem3 = nextItem;
            } else if (nextItem.category_id === 4) {
                this.selectedItem4 = nextItem;
            }
        }
    }

    /**
     * 前の写真へ
     * @param {Good} item
     */
    public turnPrevPage(item: Good): void {
        const prevItem = this.goods.find(good => {
            return good.category_id === item.category_id && good.id < item.id;
        });
        if (prevItem) {
            if (prevItem.category_id === 1) {
                this.selectedItem1 = prevItem;
            } else if (prevItem.category_id === 2) {
                this.selectedItem2 = prevItem;
            } else if (prevItem.category_id === 3) {
                this.selectedItem3 = prevItem;
            } else if (prevItem.category_id === 4) {
                this.selectedItem4 = prevItem;
            }
        }
    }

    /**
     * 品物一覧取得
     */
    private getGoods(): void {
        this.goodService.getGoods()
            .subscribe(response => {
                this.goods = response.data.map(item => new Good(item));
                // 初期選択
                this.selectedItem1 = this.goods.find(item => item.category_id === 1);
                this.selectedItem2 = this.goods.find(item => item.category_id === 2);
                this.selectedItem3 = this.goods.find(item => item.category_id === 3);
                this.selectedItem4 = this.goods.find(item => item.category_id === 4);
            });
    }
}
