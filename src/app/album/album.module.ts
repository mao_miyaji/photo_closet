import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';

import {AlbumRoutingModule} from './album-routing.module';

import {AlbumListComponent} from './album-list/album-list.component';

@NgModule({
    imports: [
        SharedModule,
        AlbumRoutingModule
    ],
    declarations: [
        AlbumListComponent
    ]
})
export class AlbumModule { }
