import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AlbumListComponent} from './album-list/album-list.component';

const ALUBM_ROUTES: Routes = [
    {
        path: 'album-list',
        component: AlbumListComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ALUBM_ROUTES)
    ],
    exports: [
        RouterModule
    ]
})
export class AlbumRoutingModule { }
