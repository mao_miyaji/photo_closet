import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AuthComponent} from './auth/auth.component';

const appRoutes: Routes = [
    {
        path: 'auth',
        component: AuthComponent
    },
    /**
     * No Target
     */
    {
        path: '**',
        redirectTo: 'auth'
    }
];

@NgModule({
  imports: [
      RouterModule.forRoot(appRoutes)
  ],
  exports: [
      RouterModule
  ]
})
export class AppRoutes { }
