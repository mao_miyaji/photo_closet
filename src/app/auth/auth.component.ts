import {Component, OnInit} from '@angular/core';
import {AuthService} from '../shared/services';
import {Router} from '@angular/router';
import {AUTH_CONF} from '../app.constants';
import {resetToken} from '../shared/libraries';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
    model: any = {};
    error: string = '';
    constructor(
        private router: Router,
        private authService: AuthService
    ) { }

    ngOnInit(): void {
        // トークンリセット
        // resetToken();
    }

    /**
     * ログインボタン押下
     */
    login(): void {
        this.authService.login(this.model.login_id, this.model.password)
            .subscribe(
                result => {
                    if (result === true) {
                        // login successful
                        this.router.navigate(['/closet-manager']);
                    } else {
                        // login failed
                        this.error = 'ログインID or パスワードが違います';
                        // this.loading = false;
                    }
                },
                () => {
                    // this.loading = false;
                    this.error = 'サーバーエラー';
                }
            );
    }
}
