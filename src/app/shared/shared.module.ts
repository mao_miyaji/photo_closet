/**
 * @angular
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from './material.module';

/**
 * Pipe
 */
import { NoimagePipe } from './pipes';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        NoimagePipe
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        NoimagePipe,
        FlexLayoutModule,
    ]
})
export class SharedModule { }
