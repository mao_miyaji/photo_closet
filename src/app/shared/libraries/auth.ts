import {LoginSuccess} from '../services';
import {AUTH_CONF} from '../../app.constants';

/**
 * Auth関連共通処理
 */

/**
 * トークン保存
 * @param {LoginSuccess} tokenResponse
 */
export function setToken(tokenResponse: LoginSuccess): void {
    localStorage.setItem(AUTH_CONF.AUTH_TOKEN_NAME, tokenResponse.access_token);
    localStorage.setItem(AUTH_CONF.AUTH_REFRESH_TOKEN_NAME, tokenResponse.refresh_token);
    localStorage.setItem(AUTH_CONF.AUTH_TOKEN_EXPIRES_NAME, tokenResponse.expires_in + '');
}

/**
 * トークン削除
 */
export function resetToken(): void {
    localStorage.removeItem(AUTH_CONF.AUTH_TOKEN_NAME);
    localStorage.removeItem(AUTH_CONF.AUTH_REFRESH_TOKEN_NAME);
    localStorage.removeItem(AUTH_CONF.AUTH_TOKEN_EXPIRES_NAME);
}

/**
 * トークンの有効期限確認
 * refreshが必要であればtrue
 * @returns {boolean}
 */
export function isTokenFresh(): boolean {
    const token = localStorage.getItem(AUTH_CONF.AUTH_TOKEN_NAME);
    const expires_time = localStorage.getItem(AUTH_CONF.AUTH_TOKEN_EXPIRES_NAME);
    if (expires_time && token) {
        const date = new Date();
        const timestamp = Math.floor(date.getTime());
        if (+expires_time > timestamp) {
            return true;
        } else {
            return false;
        }
    } else {
        return true; // tokenがなかった場合、ログイン時
    }
}
