/**
 * 日付に関する共通処理まとめ
 */

/**
 * 規定の日付フォーマットに変換
 * @param {Date} date
 * @returns {string} ex)2017-01-16
 */
export function parseDate(date: Date): string {
    const yyyy = date.getFullYear();
    const mm = ('0' + (date.getMonth() + 1)).slice(-2);
    const dd = ('0' + date.getDate()).slice(-2);
    return yyyy + '-' + mm + '-' + dd;
}

/**
 * 規定の時刻フォーマットに変換
 * @param {Date} date
 * @returns {string} ex)10:00:00
 */
export function parseTime(date: Date): string {
    const hour: string = ('0' + date.getHours()).slice(-2);
    const minutes: string = ('0' + date.getMinutes()).slice(-2);
    const seconds: string = ('0' + date.getSeconds()).slice(-2);
    return hour + ':' + minutes + ':' + seconds;
}

/**
 * 規定のdatetimeフォーマットに変換
 * @param {Date} date
 * @returns {string} ex)2017-01-16 00:00:00
 */
export function parseDateTime(date: Date): string {
    return parseDate(date) + ' ' + parseTime(date);
}
