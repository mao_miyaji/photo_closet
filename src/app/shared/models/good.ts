export class Good {
    public id: number;
    public category_id: number;
    public name: string;
    public detail: string;
    public purchase_at: string;
    public favorite_flag: number;
    public image_path: string;
    public tags?: GoodTag[];
    // 抜粋
    get excerpt(): string {
        return this.detail && this.detail.length > 15 ? this.detail.slice(0, 15) + '...' : this.detail;
    }

    constructor(good: any) {
        this.id = good.id;
        this.category_id = good.category_id;
        this.name = good.name;
        this.detail = good.detail;
        this.purchase_at = good.purchase_at;
        this.favorite_flag = good.favorite_flag;
        this.image_path = good.image_path;
        this.tags = good.tags;
    }
}

export class GoodTag {
    public id: number;
    public name: string;
    constructor(goodTag: any) {
        this.id = goodTag.id;
        this.name = goodTag.name;
    }
}
