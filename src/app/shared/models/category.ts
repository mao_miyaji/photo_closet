export class Category {
    public id: number;
    public name: string;
    constructor(category: any) {
        this.id = category.id;
        this.name = category.name;
    }
}
