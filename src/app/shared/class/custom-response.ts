export class CustomResponse<T> {
    public data?: T;
    public errors?: T;
    public total?: number;
    public status?: string;
}

/**
 * Basic SearchParam
 */
export interface SearchParam {
    [key: string]: any;
}

export interface QueryParam {
    limit?: number;
    offset?: number;
}
