/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { NoimagePipe } from './noimage.pipe';

describe('NoimagePipe', () => {
    const pipe = new NoimagePipe();
    const no_image_src = '/public/images/no_image.png';
    it('create an instance', () => {
        expect(pipe).toBeTruthy();
    });
    it('空文字', () => {
        expect(pipe.transform('')).toBe(no_image_src);
    });
    it('undefined', () => {
        expect(pipe.transform(undefined)).toBe(no_image_src);
    });
    it('null', () => {
        expect(pipe.transform(null)).toBe(no_image_src);
    });
    it('ABC', () => {
        expect(pipe.transform('ABC')).toBe('ABC');
    });
    it('add args containe', () => {
        expect(pipe.transform('BCD', 'ABC', 'BCD', 'EFD')).toBe(no_image_src);
    });
    it('add args no containe', () => {
        expect(pipe.transform('FGH', 'ABC', 'BCD', 'EFD')).toBe('FGH');
    });
});
