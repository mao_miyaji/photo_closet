import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'noimage'
})
export class NoimagePipe implements PipeTransform {

    transform(value: string, ...args: string[]): string {
        if (!value || args.indexOf(value) !== -1) {
            return '/public/images/no_image.png';
        }
        return value;
    }
}
