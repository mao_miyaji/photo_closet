import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';

import {CommonModule} from '@angular/common';

import {HTTP_INTERCEPTORS} from '@angular/common/http';

/**
 * 初期設定系&library系
 */
import {
    SpinnerService
} from './services/';

/**
*  Service Component
*  APIなど通信系コンポーネント
*/
import {
    CommonService,
    AuthInterceptor,
    AuthService,
    UserService,
    GoodService,
    GoodTagService,
    CategoryService
} from './services';

@NgModule({
    imports: [CommonModule]
})
export class CoreModule {

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [
                CommonService,
                SpinnerService,
                AuthService,
                UserService,
                GoodService,
                GoodTagService,
                CategoryService,
                {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
            ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error(
                'CoreModule is already loaded. Import it in the AppModule only');
        }
    }

}
