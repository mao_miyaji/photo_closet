import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {AUTH_CONF} from '../../app.constants';
import {CommonService} from './commons/common.service';
import {setToken} from '../libraries';

// ログイン成功時のレスポンス
export interface LoginSuccess {
    refresh_token: string;
    expires_in: number;
    access_token: string;
}

@Injectable()
export class AuthService {
    public token: string;

    constructor(private commonService: CommonService) {
        // set token if saved in local storage
        this.token = localStorage.getItem(AUTH_CONF.AUTH_TOKEN_NAME);
    }

    login(account: string, password: string): Observable<boolean> {

        const tmpObj = {account: account, password: password};
        const bodyParams = JSON.stringify(this.commonService.buildBody(tmpObj));

        return this.commonService.callRequest<LoginSuccess>('POST', this.commonService.apiPath + '/auth/login', bodyParams)
            .map((response) => {
                // login successful if there's a jwt token in the response
                const accessToken = response.access_token;
                if (accessToken) {
                    // set token property
                    this.token = accessToken;
                    // Strageに保管
                    setToken(response);
                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }
}
