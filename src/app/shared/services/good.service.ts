import {Injectable} from '@angular/core';
import {CommonService} from './commons/common.service';
import {Observable} from 'rxjs/Observable';
import {CustomResponse, QueryParam, SearchParam} from '../class/';
import {Good} from '../models';

@Injectable()
export class GoodService {

    constructor(private commonService: CommonService) {}

    /**
     * 一覧取得
     * @returns {Observable<CustomResponse<Good[]>>}
     */
    public getGoods(): Observable<CustomResponse<Good[]>> {
        const target_url = this.commonService.apiPath + '/goods';
        return this.commonService.callRequest('GET', target_url);
    }

    // /**
    //  * 検索
    //  * @param {SearchParam} searchParams
    //  * @param {QueryParam} queryParams
    //  * @returns {Observable<CustomResponse<Good[]>>}
    //  */
    // public searchGoods(searchParams: SearchParam, queryParams?: QueryParam): Observable<CustomResponse<Good[]>> {
    //     const bodyParams = JSON.stringify(this.commonService.buildBody(searchParams, queryParams));
    //     const target_url = this.commonService.apiPath + '/goods/search';
    //     return this.commonService.callRequest('POST', target_url, bodyParams);
    // }

    /**
     * 単品取得
     * @param {number} id
     * @returns {Observable<CustomResponse<Good>>}
     */
    public getGood(id: number): Observable<CustomResponse<Good>> {
        const target_url = this.commonService.apiPath + '/goods/' + id;
        return this.commonService.callRequest('GET', target_url);
    }

    /**
     * 作成
     * @param {SearchParam} addParams
     * @returns {Observable<CustomResponse<Good>>}
     */
    public addGood(addParams: SearchParam): Observable<CustomResponse<Good>> {
        const bodyParams = JSON.stringify(this.commonService.buildBody(addParams));
        const target_url = this.commonService.apiPath + '/goods';
        return this.commonService.callRequest('POST', target_url, bodyParams);
    }

    /**
     * 更新
     * @param {number} id
     * @param {SearchParam} editParams
     * @returns {Observable<CustomResponse<Good>>}
     */
    public updateGood(id: number, editParams: SearchParam): Observable<CustomResponse<Good>> {
        const bodyParams = JSON.stringify(this.commonService.buildBody(editParams));
        const target_url = this.commonService.apiPath + '/goods/' + id;
        return this.commonService.callRequest('PUT', target_url, bodyParams);
    }

    /**
     * タグから検索
     * @param {{good_tag_ids: number[]}} searchParams
     * @returns {Observable<CustomResponse<Good[]>>}
     */
    public searchGoodsByTags(searchParams: {good_tag_ids: number[]}): Observable<CustomResponse<Good[]>> {
        const bodyParams = JSON.stringify(this.commonService.buildBody(searchParams));
        const target_url = this.commonService.apiPath + '/goods/search/tag';
        return this.commonService.callRequest('POST', target_url, bodyParams);
    }
}
