import {Injectable} from '@angular/core';

@Injectable()
export class SpinnerService {

    private _selector: string = 'preloader';
    private _element: HTMLElement;

    constructor() {
        this._element = document.getElementById(this._selector);
    }

    public show(): void {
        this._element.style['display'] = 'block';
    }

    public hide(delay: number = 0): void {
        const second = delay + 1000;
        this._element.className = 'preloader-fadeout';
        this._element.style['animation-duration'] = Math.floor(second / 1000) + 's';
        setTimeout(() => {
            this._element.style['display'] = 'none';
        }, second);
    }
}
