import {Injectable} from '@angular/core';
import {CommonService} from './commons/common.service';
import {Observable} from 'rxjs/Observable';
import {CustomResponse, QueryParam, SearchParam} from '../class/';
import {Category} from '../models';

@Injectable()
export class CategoryService {

    constructor(private commonService: CommonService) {}

    /**
     * 一覧取得
     * @returns {Observable<CustomResponse<Category[]>>}
     */
    public getCategories(): Observable<CustomResponse<Category[]>> {
        const target_url = this.commonService.apiPath + '/categories';
        return this.commonService.callRequest('GET', target_url);
    }

    /**
     * 単品取得
     * @param {number} id
     * @returns {Observable<CustomResponse<Category>>}
     */
    public getCategory(id: number): Observable<CustomResponse<Category>> {
        const target_url = this.commonService.apiPath + '/categories/' + id;
        return this.commonService.callRequest('GET', target_url);
    }

    /**
     * 作成
     * @param {SearchParam} addParams
     * @returns {Observable<CustomResponse<Category>>}
     */
    public addCategory(addParams: SearchParam): Observable<CustomResponse<Category>> {
        const bodyParams = JSON.stringify(this.commonService.buildBody(addParams));
        const target_url = this.commonService.apiPath + '/categories';
        return this.commonService.callRequest('POST', target_url, bodyParams);
    }

    /**
     * 更新
     * @param {number} id
     * @param {SearchParam} editParams
     * @returns {Observable<CustomResponse<Category>>}
     */
    public updateCategory(id: number, editParams: SearchParam): Observable<CustomResponse<Category>> {
        const bodyParams = JSON.stringify(this.commonService.buildBody(editParams));
        const target_url = this.commonService.apiPath + '/categories/' + id;
        return this.commonService.callRequest('PUT', target_url, bodyParams);
    }
}
