import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpErrorResponse, HttpResponse, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {environment} from '../../../../environments/environment';
import {AUTH_CONF} from '../../../app.constants';
import {isBoolean, isArray} from 'util';
import {CustomResponse, QueryParam} from '../../class';
import {LoginSuccess} from '../auth.service';
import {setToken, resetToken, isTokenFresh} from '../../libraries';

type AllowedHttpMethod = 'POST' | 'GET' | 'PUT' | 'DELETE';

@Injectable()
export class CommonService {
    public apiPath: string;
    public method: string;
    public url: string;
    public bodyParams: any;
    /**
     * 認証失敗時に使用する再ログイン動作
     * {　display: 再ログイン画面の表示フラグ; message: 表示時のエラーメッセージ}
     */
    public reAuthSubject = new Subject<{display: boolean; error?: string}>();

    constructor(
        private http: HttpClient,
        private router: Router
    ) {
        // defalutのURL設定
        this.apiPath = environment.api_server;
    }

    public callRequest<T>(method: AllowedHttpMethod, url: string, bodyParams?: any, options?: any): Observable<T>;
    public callRequest(method: AllowedHttpMethod, url: string, bodyParams?: any, options?: any): Observable<any> {
        // if (!isTokenFresh()) {
        //     this.router.navigate(['/auth']);
        //     return Observable.empty();
        // }

        this.method = method;
        this.url = url;
        this.bodyParams = bodyParams;

        let request: Observable<any>;

        if (!options) {
            options = {responseType: 'json'};
        }

        if (method === 'POST') {
            request = this.http.post(url, bodyParams, options);
        } else if (method === 'GET') {
            request = this.http.get(url, options);
        } else if (method === 'PUT') {
            request = this.http.put(url, bodyParams, options);
        } else if (method === 'DELETE') {
            request = this.http.delete(url, options);
        }

        return request
            .retryWhen(error => error.switchMap((err: HttpErrorResponse) => {
                return this.extractError(err);
            }))
            .retry(2)
            .finally(() => {
                console.log('After the request...');
            });
    }

    private handleError(error: HttpErrorResponse | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof HttpErrorResponse) {
            if (error.status === 404 || error.status === 500) {
                const err = error || {};
                errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
            } else {
                if (error.error && typeof error.error === 'object' && error.error.message) {
                    // Exceptionメッセージがあればそれを使用する
                    errMsg = `${error.status} - ${error.error.message}`;
                } else if (error.message) {
                    // なければエラーメッセージを使用する
                    errMsg = `${error.status} - ${error.message || error.statusText}`;
                } else {
                    const err = error.error || JSON.stringify(error);
                    errMsg = `${error.status} - ${error.statusText || ''}  ${err}`;
                }
            }
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }

    /**
     * 検索クエリ生成
     *
     * @param param
     * @param {QueryParam} query
     * @returns {any}
     */
    public buildBody(param?: any, query?: QueryParam): any {
        const tmpObj: any = {};
        if (param) {
            if (query) {
                tmpObj.query = query;
            }
            for (const key in param) {
                // 配列の場合はそのまま返す
                if (isArray(param[key])) {
                    continue;
                }
                // booleanの時は0,1に変更
                if (isBoolean(param[key])) {
                    if (param[key]) {
                        param[key] = 1;
                    } else {
                        param[key] = 0;
                    }
                }
            }
            tmpObj.param = param;
            return tmpObj;
        } else {
            if (query) {
                tmpObj.query = query;
            }
            return tmpObj;
        }
    }

    /**
     * 401返った時にTokenをリフレッシュするために行う。
     * 期限切れの場合のみ受けつけ
     *
     * @returns {Map<any>}
     */
    refresh(): Observable<any> {
        const refreshToken = localStorage.getItem(AUTH_CONF.AUTH_REFRESH_TOKEN_NAME);
        const tmpObj = {refresh_token: refreshToken};
        const bodyParams = JSON.stringify(this.buildBody(tmpObj));

        return this.http.post<LoginSuccess>(this.apiPath + '/auth/refresh_token', bodyParams)
            .map((response) => {
                // login successful if there's a jwt token in the response
                const accessToken = response.access_token;
                if (accessToken) {
                    // Strageに保管
                    setToken(response);
                    console.log('refresh ok!');
                    // return true to indicate successful login
                    return true;
                } else {
                    // トークンリセット
                    resetToken();
                    // return false to indicate failed login
                    return false;
                }
            })
            .catch(err => {
                // リフレッシュでこけた場合は再ログインを求める。
                return this.authFailed(`認証に失敗しました。`);
            });
    }

    /**
     * handle error response
     * @param  {HttpErrorResponse | any}         error
     * @return {Observable<any>}
     */
    private extractError(error: HttpErrorResponse | any): Observable<any> {
        switch (error.status) {
            case 401:
                return this.unauthorizedError(error);
            default:
                return this.handleError(error);
        }
    }

    /**
     * 認証失敗時の動作
     * @param  {HttpErrorResponse}         error
     * @return {Observable<any>}
     */
    private unauthorizedError(error: HttpErrorResponse): Observable<any> {
        switch (error.error) {
            // case UNAUTHORIZED_MESSAGE.TOKEN_REJECT:
            //     return this.refresh();
            // case UNAUTHORIZED_MESSAGE.MULTI_LOGIN:
            //     // 再認証
            //     return this.authFailed(`多重ログインを検知しました。`);
            default:
                return this.handleError(error);
        }
    }

    /**
     * 認証失敗時の動作
     * @param {string} title
     * @param {string} text
     */
    private authFailed(error: string): Observable<any> {
        // 本筋のストリームを完了させる
        return Observable.throw(error);
    }
}
