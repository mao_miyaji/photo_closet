import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../../environments/environment';
import {AUTH_CONF} from '../../../app.constants';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const headers: any = {
            'Accept': 'application/json',
            'Content-Type': environment['content-type']
        };
        // const token = localStorage.getItem(AUTH_CONF.AUTH_TOKEN_NAME);
        // if (token) {
            //     headers['Authorization'] = 'Bearer ' + token;
            // }
        const req = request.clone({setHeaders: headers});
        return next.handle(req);
    }
}
