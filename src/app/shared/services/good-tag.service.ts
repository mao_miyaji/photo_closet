import {Injectable} from '@angular/core';
import {CommonService} from './commons/common.service';
import {Observable} from 'rxjs/Observable';
import {CustomResponse, QueryParam, SearchParam} from '../class/';
import {GoodTag} from '../models';

@Injectable()
export class GoodTagService {

    constructor(private commonService: CommonService) {}

    /**
     * 一覧取得
     * @returns {Observable<CustomResponse<GoodTag[]>>}
     */
    public getGoodTags(): Observable<CustomResponse<GoodTag[]>> {
        const target_url = this.commonService.apiPath + '/good-tags';
        return this.commonService.callRequest('GET', target_url);
    }
    /**
     * 単品取得
     * @param {number} id
     * @returns {Observable<CustomResponse<GoodTag>>}
     */
    public getGoodTag(id: number): Observable<CustomResponse<GoodTag>> {
        const target_url = this.commonService.apiPath + '/good-tags/' + id;
        return this.commonService.callRequest('GET', target_url);
    }

    /**
     * 作成
     * @param {SearchParam} addParams
     * @returns {Observable<CustomResponse<Good>>}
     */
    public addGoodTag(addParams: SearchParam): Observable<CustomResponse<GoodTag>> {
        const bodyParams = JSON.stringify(this.commonService.buildBody(addParams));
        const target_url = this.commonService.apiPath + '/good-tags';
        return this.commonService.callRequest('POST', target_url, bodyParams);
    }

    /**
     * 更新
     * @param {number} id
     * @param {SearchParam} editParams
     * @returns {Observable<CustomResponse<Good>>}
     */
    public updateGoodTag(id: number, editParams: SearchParam): Observable<CustomResponse<GoodTag>> {
        const bodyParams = JSON.stringify(this.commonService.buildBody(editParams));
        const target_url = this.commonService.apiPath + '/good-tags/' + id;
        return this.commonService.callRequest('PUT', target_url, bodyParams);
    }
}
