import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {AUTH_CONF} from '../../app.constants';
import {isTokenFresh} from '../libraries';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) {
    }

    canActivate() {
        if (localStorage.getItem(AUTH_CONF.AUTH_TOKEN_NAME)) {
            if (isTokenFresh()) {
                this.router.navigate(['/auth']);
                return false;
            }
            return true;
        }
        // not logged in so redirect to login page
        this.router.navigate(['/auth']);
        return false;
    }
}
