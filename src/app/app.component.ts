import {Component, AfterViewInit} from '@angular/core';
import {SpinnerService, CommonService} from './shared/services';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
    title = 'app';

    constructor(
        private _spinner: SpinnerService,
        private commonService: CommonService
    ) { }

    ngAfterViewInit() {
        this._spinner.hide(3000);
    }
}
