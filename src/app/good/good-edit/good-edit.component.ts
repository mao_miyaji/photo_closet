import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormControl} from '@angular/forms';
import {Good} from '../../shared/models';
import {CustomResponse} from '../../shared/class';
import {GoodService} from '../../shared/services';
import {parseDateTime} from '../../shared/libraries';

@Component({
    selector: 'app-good-edit',
    templateUrl: './good-edit.component.html',
    styleUrls: ['./good-edit.component.css']
})
export class GoodEditComponent implements OnInit {
    public good: Good;
    // 編集モード
    public isEditMode: boolean = false;
    // 編集用フォーム
    public formData: any = {};
    // datepickerに渡す購入日
    public purchase_at: FormControl = new FormControl();

    constructor(
        private route: ActivatedRoute,
        private goodService: GoodService
    ) { }

    ngOnInit(): void {
        this.route.data.subscribe((data: {good: CustomResponse<Good>}) => {
            this.good = new Good(data.good.data);
            this.formData = Object.assign({}, this.good);
            if (this.formData.purchase_at) {
                this.purchase_at = new FormControl(new Date(this.formData.purchase_at));
            }
        });
    }

    /**
     * 保存ボタン押下
     */
    public onSave(): void {
        this.formData.purchase_at = this.purchase_at.value ? parseDateTime(new Date(this.purchase_at.value)) : null;
        this.updateGood();
    }

    /**
     * 更新
     */
    private updateGood(): void {
        this.goodService.updateGood(this.good.id, this.formData)
            .subscribe(
                response => {
                    this.good = new Good(response.data);
                    this.isEditMode = false;
                    this.formData = Object.assign({}, this.good);
                },
                () => console.log('error')
            );
    }
}
