import {Injectable} from '@angular/core';
import {Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import {CustomResponse} from '../shared/class';
import {GoodService} from '../shared/services';
import {Good} from '../shared/models';

@Injectable()
export class GoodResolver implements Resolve<CustomResponse<Good>> {
    constructor(private goodService: GoodService, private router: Router) { }

    resolve(route: ActivatedRouteSnapshot) {
        const id = route.params['id'];

        return this.goodService.getGood(id)
                .catch((err) => {
                    this.router.navigate(['/closet-manager']);
                    return err;
                });
    }
}
