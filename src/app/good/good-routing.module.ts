import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {GoodResolver} from './good-resolver.service';

import {GoodAddComponent} from './good-add/good-add.component';
import {GoodEditComponent} from './good-edit/good-edit.component';

const GOOD_ROUTES: Routes = [
    {
        path: 'good-add',
        component: GoodAddComponent
    },
    {
        path: 'good-edit/:id',
        component: GoodEditComponent,
        resolve: {good: GoodResolver}
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(GOOD_ROUTES)
    ],
    exports: [
        RouterModule
    ]
})
export class GoodRoutingModule { }
