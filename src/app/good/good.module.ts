import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';

import {GoodRoutingModule} from './good-routing.module';
import {GoodResolver} from './good-resolver.service';

import {GoodAddComponent} from './good-add/good-add.component';
import {GoodEditComponent} from './good-edit/good-edit.component';

@NgModule({
    imports: [
        SharedModule,
        GoodRoutingModule
    ],
    declarations: [
        GoodAddComponent,
        GoodEditComponent
    ],
    providers: [
        GoodResolver
    ]
})
export class GoodModule { }
