/**
 *  Basic Module
 */
import './rxjs-operators';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {NgModule, LOCALE_ID} from '@angular/core';
import {MAT_DATE_LOCALE} from '@angular/material';

/**
 * Locale
 */
import {registerLocaleData} from '@angular/common';
import localeJa from '@angular/common/locales/ja';
registerLocaleData(localeJa, 'ja');

/**
 * App
 */
import {AppComponent} from './app.component';
import {AppRoutes} from './app.routing';

/**
 * Common Module
 */
import {SharedModule} from './shared/shared.module';
import {CoreModule} from './shared/core.module';

/**
 * Page Component
 */
import {HeaderComponent} from './header/header.component';
import {AuthComponent} from './auth/auth.component';

/**
 * Page Module
 */
import {ClosetModule} from './closet/closet.module';
import {GoodModule} from './good/good.module';
import {AlbumModule} from './album/album.module';

@NgModule({
    declarations: [
        AppComponent,
        AuthComponent,
        HeaderComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        SharedModule,
        ClosetModule,
        GoodModule,
        AlbumModule,
        AppRoutes,
        HttpClientModule,
        CoreModule.forRoot()
    ],
    providers: [
        {provide: LOCALE_ID, useValue: 'ja'},
        {provide: MAT_DATE_LOCALE, useValue: 'ja'}
    ],
    entryComponents: [AppComponent],
    bootstrap: [AppComponent]
})
export class AppModule { }
