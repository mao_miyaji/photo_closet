// 認証関連
export const AUTH_CONF = {
    // JWT保存名
    AUTH_TOKEN_NAME: 'access_token',
    // JWTリフレッシュ用
    AUTH_REFRESH_TOKEN_NAME: 'refresh_token',
    // JWT有効期限保存先
    AUTH_TOKEN_EXPIRES_NAME: 'token_expires'
};
