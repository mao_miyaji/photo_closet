import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ClosetManagerComponent} from './closet-manager/closet-manager.component';

const CLOSET_ROUTES: Routes = [
    {
        path: 'closet-manager',
        component: ClosetManagerComponent,
        // canActivate: [AuthGuard],
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(CLOSET_ROUTES)
    ],
    exports: [
        RouterModule
    ]
})
export class ClosetRoutingModule { }
