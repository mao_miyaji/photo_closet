import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClosetManagerComponent } from './closet-manager.component';

describe('ClosetManagerComponent', () => {
  let component: ClosetManagerComponent;
  let fixture: ComponentFixture<ClosetManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClosetManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClosetManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
