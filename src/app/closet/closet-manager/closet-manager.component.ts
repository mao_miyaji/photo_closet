import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {GoodService, GoodTagService, CategoryService} from '../../shared/services';
import {QueryParam} from '../../shared/class';
import {Good, GoodTag, Category} from '../../shared/models';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';

@Component({
    selector: 'app-closet-manager',
    templateUrl: './closet-manager.component.html',
    styleUrls: ['./closet-manager.component.css']
})
export class ClosetManagerComponent implements OnInit {
    // 品物一覧
    public goods: Good[] = [];
    // タグ一覧
    public goodTags: GoodTag[] = [];
    // カテゴリ一覧
    public categories: any = {};

    myControl = new FormControl();
    filteredOptions: Observable<GoodTag[]>;

    constructor(
        private goodService: GoodService,
        private goodTagService: GoodTagService,
        private categoryService: CategoryService
    ) { }

    ngOnInit(): void {
        this.getGoods();
        this.getGoodTags();
        this.getCategories();

        this.filteredOptions = this.myControl.valueChanges
            .pipe(
                startWith<string | GoodTag>(''),
                map(value => typeof value === 'string' ? value : value.name),
                map(name => name ? this.filter(name) : this.goodTags.slice())
            );
    }

    filter(name: string): GoodTag[] {
        return this.goodTags.filter(option =>
            option.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }

    displayFn(tag?: GoodTag): string | undefined {
        return tag ? tag.name : undefined;
    }

    /**
     * お気に入り変更
     * @param {Good} item
     */
    public changeFavorite(item: Good): void {
        const param = {
            favorite_flag: item.favorite_flag ? 0 : 1
        };
        this.updateGood(item.id, param);
    }

    /**
     * 更新
     * @param {number} id
     * @param params
     */
    private updateGood(id: number, params: {[key: string]: any}): void {
        this.goodService.updateGood(id, params)
            .subscribe(() => {
                this.getGoods();
            });
    }

    /**
     * 品物一覧取得
     */
    private getGoods(): void {
        this.goodService.getGoods()
            .subscribe(response => {
                this.goods = response.data.map(item => new Good(item));
            });
    }

    /**
     * タグ一覧取得
     */
    private getGoodTags(): void {
        this.goodTagService.getGoodTags()
            .subscribe(response => {
                this.goodTags = response.data.map(item => new GoodTag(item));
            });
    }

    /**
     * カテゴリ一覧取得
     */
    private getCategories(): void {
        this.categoryService.getCategories()
            .subscribe(response => {
                response.data.forEach(item => this.categories[item.id] = item.name);
            });
    }
}
