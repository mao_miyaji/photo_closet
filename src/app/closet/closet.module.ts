import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';

import {ClosetRoutingModule} from './closet-routing.module';
import {ClosetManagerComponent} from './closet-manager/closet-manager.component';

@NgModule({
    imports: [
        SharedModule,
        ClosetRoutingModule
    ],
    declarations: [
        ClosetManagerComponent
    ]
})
export class ClosetModule { }
